function createCard(
	name,
	description,
	pictureUrl,
	formattedStartTime,
	formattedEndTime,
	location
) {
	return `
	<div class="shadow p-3 mb-5 bg-body-tertiary rounded">
        <div class="card">
            <img src="${pictureUrl}" class="card-img-top">
            <div class="card-body">
                <h5 class="card-title">${name}</h5>
				<h6 class="card-subtitle mb-2 text-muted">${location}</h6>
                <p class="card-text">${description}</p>
            </div>
        </div>
		<div class="card-footer">
			${formattedStartTime} - ${formattedEndTime}
		</div>
	</div>
    `;
}

window.addEventListener("DOMContentLoaded", async () => {
	const url = "http://localhost:8000/api/conferences/";

	try {
		const response = await fetch(url);

		// if the response is not a 200 code
		if (!response.ok) {
			// create a variable to store the bootstrap
			const errorMessage = `
				<div class="alert alert-danger" role="alert">
					Error getting the data from the API
				</div>
			`;
			// create a variable to store an empty div within
			const errorContainer = document.createElement("div");
			// store the bootstrap div within the empty div you created
			errorContainer.innerHTML = errorMessage;

			// create a variable to find the first child element of body
			const firstChild = document.body.firstChild;
			// insert your div as the first child element of the body to appear at the top of the page
			document.body.insertBefore(errorContainer, firstChild);
		} else {
			const data = await response.json();

			let counter = 0; // keeps track of which column we want our content in

			for (let i = 0; i < data.conferences.length; i++) {
				const detailUrl = `http://localhost:8000${data.conferences[i].href}`; // ${conference.href}
				const detailResponse = await fetch(detailUrl);

				if (detailResponse.ok) {
					const details = await detailResponse.json();
					const title = details.conference.name;
					const description = details.conference.description;
					const pictureUrl = details.conference.location.picture_url;
					const startTime = details.conference.starts;
					const endTime = details.conference.ends;
					const location = details.conference.location.name;

					const options = {
						year: "numeric",
						month: "2-digit",
						day: "2-digit",
					};

					const formattedStartTime = new Date(
						startTime
					).toLocaleDateString(options);
					const formattedEndTime = new Date(
						endTime
					).toLocaleDateString(options);

					const html = createCard(
						title,
						description,
						pictureUrl,
						formattedStartTime,
						formattedEndTime,
						location
					);
					const columns = document.querySelectorAll(".col"); // columns is a NodeList of all locations where 'col' appears
					const column = columns[counter]; // column is an object; grabs the location from the querySelector (div col 0, div col 1, div col 2)

					column.innerHTML += html; // put the card information in html into that specific column
					counter++;
					if (counter === 3) {
						counter = 0;
					}
				}
			}
		}
	} catch (e) {
		// Figure out what to do if an error is raised
		console.error(e);
	}
});
