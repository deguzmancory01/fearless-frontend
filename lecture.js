const clueDiv = document.querySelector("#clue");
console.log(clueDiv);

const url = "https://jservice.xyz/api/random-clue";
const response = await fetch(url);

function createClueHtml(category, question, answer) {
	// A string that looks like HTML. We will convert to HTML
	return `
        <div>
            <h2>${category}</h2>
            <p><b>Question</b>: ${question}</p>
            <p><b>Answer</b>: ${answer}</p>
        </div>
    `;
}

if (response.ok) {
	console.log(response);

	const data = await response.json();
	// console.log(data);

	const category = data.category.title;
	const question = data.question;
	const answer = data.answer;
	const html = createClueHtml(category, question, answer);
	// console.log(html);

	clueDiv.innerHTML = html;
} else {
	console.error("Got an error in the response.");
}

// POPULATE CATEGORY SELECT
// http request - get req for the categories
const catUrl = "https://jservice.xyz/api/categories";
let catRes = await fetch(catUrl);

if (catRes.ok) {
	let catArr = await catRes.json(); // This json list is too big, lets get a slice
	catArr = catArr.categories.slice(0, 100);
	// console.log(catArr);
	// add options in the dropdown/select
	for (let cat of catArr) {
		// iterate over list of categories
		// craft option tag as a string for each cat
		let { id, title } = cat;
		// let opt_str = `<option value="${id}">${title}</option>`; // This is one way to create an element
		// This is an alternative to the above line to create an element
		const option = document.createElement("option");
		option.value = id;
		option.innerHTML = title;
		// queryselect the select
		const selectEl = document.querySelector("#categoryId");
		// add it into the html for display
		selectEl.append(option);
	}
} else {
	console.log("error fetching categories");
}

// SUBMIT FORM
let form = document.querySelector("form");
// console.log(form);
form.addEventListener("submit", async function (e) {
	// e stands for the event itself
	e.preventDefault();
	// let question = document.querySelector('#question_1').value
	// console.log(question)
	// easier way to the above?
	let postUrl = "https://jservice.xyz/api/clues";
	const formData = new FormData(form); // want to convert to object then to json
	const formObj = Object.fromEntries(formData); // FormData.entries() returns an iterator that iterates through all key/value pairs contained in the FormData
	let submitResponse = await fetch(postUrl, {
		method: "POST",
		headers: {
			"Content-Type": "application/json",
		},
		body: JSON.stringify(formObj),
	});
	console.log(submitResponse.ok);
	let submitResponseJson = await submitResponse.json();
	console.log(submitResponseJson);
});
